package com.mygdx.game.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.mygdx.game.Game;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		Game gra=new Game();
                
                config.height=gra.stala*gra.y+10;
		config.width=gra.stala*gra.x+10;
		new LwjglApplication(gra, config);
	}
}
