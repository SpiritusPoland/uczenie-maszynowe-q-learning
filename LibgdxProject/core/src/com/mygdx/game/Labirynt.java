package com.mygdx.game;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.io.File;
import java.util.Scanner;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;

/**
 *
 * @author Łukasz
 */
public class Labirynt
{

    Cell tablica[][];
    int wymiarX;
    int wymiarY;
    int sciany=200;

    Labirynt(int x, int y)
        {
        wymiarX = x;
        wymiarY = y;
        tablica = new Cell[x][y];
        for (int i = 0; i < wymiarX; i++)
        {
            for (int j = 0; j < wymiarY; j++)
            {
                tablica[i][j] = new Cell(i, j);
            }
        }
         generate();
        }

    public final void generate()//funkcja generuje Labirynt
        {
        int ruch;
        Random losowanie = new Random();
        int poczatkowyX = losowanie.nextInt(this.wymiarX);
        int poczatkowyY = losowanie.nextInt(this.wymiarX);
        List<Cell> lista = new ArrayList<Cell>();
        lista.add(tablica[poczatkowyX][poczatkowyY]);
        lista.get(0).visit();
        while (lista.size() >= 1)
        {
            boolean gora = false;
            boolean dol = false;
            boolean lewo = false;
            boolean prawo = false;

            int ostatniElement = lista.size() - 1;
            int obecnyX = lista.get(lista.size() - 1).coordinateX();//przypisuje współrzędna X z ostatniego elementu listy
            int obecnyY = lista.get(lista.size() - 1).coordinateY();
            do
            {
                ruch = losowanie.nextInt(4);
                if (ruch == 0)
                {
                    gora = true;
                }
                if (ruch == 1)
                {
                    prawo = true;
                }
                if (ruch == 2)
                {
                    dol = true;
                }
                if (ruch == 3)
                {
                    lewo = true;
                }
                if (gora == true && dol == true && lewo == true && prawo == true)//warunek by pętla losująca ruch nie była nieskończona
                {
                    lista.remove(ostatniElement);
                    break;
                }
            } while (canMove(ruch, obecnyX, obecnyY) == false);
            if (gora == true && dol == true && lewo == true && prawo == true)//ponowna iteracja głównego while
            {
                continue;
            } else
            {
                if (ruch == 0)//ruch w górę
                {
                    tablica[obecnyX][obecnyY + 1].visit();
                    lista.add(tablica[obecnyX][obecnyY + 1]);
                    tablica[obecnyX][obecnyY].deleteWall(0);
                    tablica[obecnyX][obecnyY + 1].deleteWall(2);
                }
                if (ruch == 1)//ruch w prawo
                {
                    tablica[obecnyX + 1][obecnyY].visit();
                    lista.add(tablica[obecnyX + 1][obecnyY]);
                    tablica[obecnyX][obecnyY].deleteWall(1);
                    tablica[obecnyX + 1][obecnyY].deleteWall(3);
                }
                if (ruch == 2)//ruch w dół
                {
                    tablica[obecnyX][obecnyY - 1].visit();
                    lista.add(tablica[obecnyX][obecnyY - 1]);
                    tablica[obecnyX][obecnyY].deleteWall(2);
                    tablica[obecnyX][obecnyY - 1].deleteWall(0);
                }
                if (ruch == 3)//ruch w lewo
                {
                    tablica[obecnyX - 1][obecnyY].visit();
                    lista.add(tablica[obecnyX - 1][obecnyY]);
                    tablica[obecnyX][obecnyY].deleteWall(3);
                    tablica[obecnyX - 1][obecnyY].deleteWall(1);
                }
            }
        }
        for(int i=0;i<sciany;i++)
        {
          this.removeRandom();
        }

        }
    private void removeRandom()
    {
        Cell celka;
        Random random=new Random();
        int x;
        int y; 
        int wall;
        do{
        y=random.nextInt(this.wymiarY);
        x=random.nextInt(this.wymiarX);
        wall=random.nextInt(4);
        }while(!canRemove(x,y,wall));
        celka=this.getCell(x,y);
        celka.deleteWall(wall);
        if(wall==0)
            y++;        
        if(wall==1)
            x++;
        if(wall==2)
            y--;
        if(wall==3)
            x--;
        celka=this.getCell(x, y);
        celka.deleteWall((wall+2)%4);
            
    }
    private boolean canRemove(int x,int y, int wall)
    {
      if(x==0&&wall==3)
        return false;
      else if(x==(this.wymiarX-1) && wall==1)
          return false;
      else if(y==0&&wall==2)
          return false;
      else if(y==this.wymiarY-1 && wall==0)
        return false;
      else
          return true;
          
          
      
    }
    private boolean canMove(int ruch, int x, int y)
        {

        Cell cellka;
        if (ruch == 0)//oznacza przejście do komórki wyżej
        {
            try
            {
                cellka = tablica[x][y + 1];
                if (cellka.isVisited())
                {
                    return false;
                }
            } catch (IndexOutOfBoundsException e)
            {
                return false;
            }
        }
        if (ruch == 1)//oznacza przejście do komórki po prawej
        {
            try
            {
                cellka = tablica[x + 1][y];
                if (cellka.isVisited())
                {
                    return false;
                }
            } catch (IndexOutOfBoundsException e)
            {
                return false;
            }
        }
        if (ruch == 2)//oznacza przejście do komórki w dół
        {
            try
            {
                cellka = tablica[x][y - 1];
                if (cellka.isVisited())
                {
                    return false;
                }
            } catch (IndexOutOfBoundsException e)
            {
                return false;
            }
        }
        if (ruch == 3)//oznacza przejście do komórki w lewo;
        {
            try
            {
                cellka = tablica[x - 1][y];
                if (cellka.isVisited())
                {
                    return false;
                }
            } catch (IndexOutOfBoundsException e)
            {
                return false;
            }
        }
        return true;
        }

    public Cell getCell(int x, int y)//funkcja pobiera obiekt Cell z tablicy obiektów 
        {
        return tablica[x][y];
        }

    public boolean open()//funkcja zczytuje dane na temat labiryntu z pliku
        {
        String linia;
        File plik = new File("game.txt");
        if (plik.exists())
        {
            try
            {
                Scanner read = new Scanner(plik);
                int i = 0;
                while (read.hasNextLine())
                {
                    if (i == 0)
                    {
                        int wymX = read.nextInt();
                        this.setX(wymX);
                        i++;
                    }
                    if (i == 1)
                    {
                        int wymY = read.nextInt();
                        this.setY(wymY);
                        tablica = new Cell[this.wymiarX][this.wymiarY];
                        for (int x = 0; x < this.wymiarX; x++)
                        {
                            for (int y = 0; y < this.wymiarY; y++)
                            {
                                tablica[x][y] = new Cell(x, y);
                            }
                        }
                        i++;
                    }
                    if (i > 1)
                    {
                        int linie = 2;
                        read.nextLine();
                        for (int x = 0; x < this.wymiarX; x++)
                        {
                            for (int y = 0; y < this.wymiarY; y++)
                            {
                                //linia=read.nextLine();
                                linie++;
                                linia = read.nextLine();
                                tablica[x][y].setProperties(linia);
                            }
                        }
                        break;
                    }

                }

            } catch (FileNotFoundException e)
            {
                return false;
            }
            return true;
        } else
        {
            return false;
        }

        }

    public boolean save()//zapis do pliku
        {
        File plik = new File("game.txt");
        if (!plik.exists())
        {
            try
            {
                plik.createNewFile();
            } catch (IOException e)
            {
                return false;
            }
        }
        PrintWriter zapis;
        try
        {
            zapis = new PrintWriter(plik);
        } catch (FileNotFoundException e)
        {
            return false;
        }
        zapis.println(wymiarX);
        zapis.println(wymiarY);
        for (int i = 0; i < wymiarX; i++)
        {
            for (int j = 0; j < wymiarY; j++)
            {
                zapis.println(this.tablica[i][j].zapis());
            }
        }
        zapis.close();
        return true;
        }

    private void setX(int x)//ustawia wymiar X
        {
        this.wymiarX = x;
        }

    private void setY(int y)//ustawia wymiar Y
        {
        this.wymiarY = y;
        }

}
