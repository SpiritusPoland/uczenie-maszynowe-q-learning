package com.mygdx.game;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Rectangle;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class GameObject extends Rectangle
{
   
    int labiryntSizeX;
    int labiryntSizeY;
    int sizeX;
    int sizeY;
    int coordinateX;
    int coordinateY;
    boolean exists = true;
    List<int[]> movelist = new ArrayList<int[]>();//lista koordynatów dla poszczególnych kroków jakie były wykonywane.

    Texture textura;

    public GameObject(int x, int y)//do konstruktora przekazujemy wymiary labiryntu
        {
        labiryntSizeX = x;
        labiryntSizeY = y;
        Random losuj = new Random();
        this.x = losuj.nextInt(labiryntSizeX);
        this.y = losuj.nextInt(labiryntSizeY);
        coordinateX = (int) this.x;
        coordinateY = (int) this.y;
        }

    public Texture getTexture()
        {
        return textura;
        }

    protected String CheckSystem()//sprawdza system na jakim sie znajdujemy i ustawia ścieżkę do pliku
        {
        String a;
        String OS = System.getProperty("os.name");
        if (OS.equals("Windows 10"))
        {
            a = "";
            return a;
        } else
        {
            a = "core/assets/";
            return a;
        }

        }

    public void moveObject(Labirynt labirynt)
        {
        int numberOfCell = 0;
        float losowana;
        Cell komorka;
        komorka = labirynt.getCell(coordinateX, coordinateY);
        List<Cell> lista = new ArrayList<Cell>();
        if (komorka.isGornaSciana() == false)
        {
            lista.add(labirynt.getCell(coordinateX, coordinateY + 1));
            numberOfCell++;
        }
        if (komorka.isPrawaSciana() == false)
        {
            lista.add(labirynt.getCell(coordinateX + 1, coordinateY));
            numberOfCell++;
        }
        if (komorka.isDolnaSciana() == false)
        {
            lista.add(labirynt.getCell(coordinateX, coordinateY-1));
            numberOfCell++;
        }
        if (komorka.isLewaSciana() == false)
        {
            lista.add(labirynt.getCell(coordinateX-1, coordinateY));
            numberOfCell++;
        }
        float suma=0;
        for(int i=0;i<numberOfCell;i++)
        {
            suma+=lista.get(i).wartosc;
        }
        Random losuj=new Random();
        losowana=losuj.nextFloat()*suma;//wylosowana liczba z przedziału;
        float sprawdz=0;
        for(int i=0;i<lista.size();i++)
        {
        sprawdz+=lista.get(i).wartosc;    
        if(losowana<sprawdz)
        {
            int[] ruch=new int[2];
            ruch[0]=this.coordinateX;
            ruch[1]=this.coordinateY;
            movelist.add(ruch);
            this.x=lista.get(i).getX();
            this.y=lista.get(i).getY();
            this.coordinateX=(int)this.x;
            this.coordinateY=(int)this.y;
            
            break;
        }
        }
        }
    public void reset()//funkcja resetująca obiekt do stanu początkowego
        {
        if(!(0==(this.movelist.size())))
        {
        this.exists=true;
        this.x=movelist.get(0)[0];
        this.y=movelist.get(0)[1];
        this.coordinateX=(int)this.x;
        this.coordinateY=(int)this.y;
        movelist.clear();
        }
    }
}
