package com.mygdx.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
/*
STEROWANIE:
W - przyspiesza tempo symulacji;
S - zwalnia tempo symulacji;
R - resetuje symulacje;
Esc - zamyka aplikacje;




*/
public class Game extends ApplicationAdapter
{
    
    public int stala=30;
    float skala=(float)stala/50;
    float predkosc;
    float timer=0;
    SpriteBatch batch;
    public int x = 20;
    public int y = 20;
    Labirynt labirynt = new Labirynt(x, y);//tworzymy labirynt
    Cell celka;
    ShapeRenderer sr;//obiekt renderujacy	
    int mouseCount = 3, catCount = 4, cheeseCount = 15;
    Mouse myszki[];
    Cat kotki[];
    Cheese sery[];

    @Override
    public void create()//fukcja wywoływana raz
        {
         predkosc=1;
        System.out.println("Prędkość symulacji ustawiona jest na:"+predkosc);
        batch = new SpriteBatch();
        Gdx.gl.glLineWidth(10);
        sr = new ShapeRenderer();
        sr.setColor(Color.BLACK);
        myszki = new Mouse[mouseCount];
        kotki = new Cat[catCount];
        sery = new Cheese[cheeseCount];
        this.ifCreated();
        this.createObjects();
        }

    @Override
    public void render()//nieskończona pętla
        {
        predkosc=this.speed(predkosc);
        this.przyciski();
        Gdx.gl.glClearColor(255, 255, 255, 1);//kolor tła
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        rysujLabirynt(sr);
        batch.begin();
        this.rysujObiekty(batch);
        batch.end();
        
      
        if((timer+=Gdx.graphics.getDeltaTime())>predkosc)//ruch co daną ilość czasu
        {
        for(int i=0;i<mouseCount;i++)
        {
            myszki[i].move(labirynt, sery, kotki);
        }
        for(int i=0;i<catCount;i++)
        {
            kotki[i].move(labirynt,myszki);
        }
        timer=0;
        }
        
        }

//	@Override
//	public void dispose() {
//		batch.dispose();
//		img.dispose();
//	}
    public void rysujLabirynt(ShapeRenderer sr)
        {
        sr.setAutoShapeType(true);
        sr.begin(ShapeRenderer.ShapeType.Line);
        for (int i = 0; i < labirynt.wymiarX; i++)
        {
            for (int j = 0; j < labirynt.wymiarY; j++)
            {
                celka = labirynt.getCell(i, j);
                for (int k = 0; k < 4; k++)
                {
                    switch (k)
                    {
                        case 0:
                            if (celka.getWall(k))
                            {
                                sr.line(i * stala, j * stala + stala, i * stala + stala, j * stala + stala);
                            }
                            break;
                        case 1:
                            if (celka.getWall(k))
                            {
                                sr.line(i * stala + stala, j * stala, i * stala + stala, j * stala + stala);
                            }
                            break;
                        case 2:
                            if (celka.getWall(k))
                            {
                                sr.line(i * stala, j * stala, i * stala + stala, j * stala);
                            }
                            break;
                        case 3:
                            if (celka.getWall(k))
                            {
                                sr.line(i * stala, j * stala, i * stala, j * stala + stala);
                            }
                            break;
                    }
                }
            }
        }
        sr.end();
        }

    public void rysujObiekty(SpriteBatch batch)
        {
        for (int i = 0; i < mouseCount; i++)
        {
            if (myszki[i].exists)
            {
                batch.draw(myszki[i].getTexture(), myszki[i].x * stala + 5, myszki[i].y * stala + 5, (int)40*skala, (int)40*skala);
            }
        }
        for (int i = 0; i < catCount; i++)
        {
            if (kotki[i].exists)
            {
                batch.draw(kotki[i].getTexture(), kotki[i].x * stala + 5, kotki[i].y * stala + 5, (int)40*skala, (int)40*skala);
            }
        }
        for (int i = 0; i < cheeseCount; i++)
        {
            if (sery[i].exists)
            {
                batch.draw(sery[i].getTexture(), sery[i].x * stala + 5, sery[i].y * stala + 5, (int)40*skala, (int)40*skala);
            }
        }

        }

    public void createObjects()//funkcja towrzy obiekty
        {
        for (int i = 0; i < mouseCount; i++)
        {
            myszki[i] = new Mouse(this.x, this.y);
        }
        for (int i = 0; i < catCount; i++)
        {
            kotki[i] = new Cat(this.x, this.y);
        }
        for (int i = 0; i < cheeseCount; i++)
        {
            sery[i] = new Cheese(this.x, this.y);
        }
        }

    public void ifCreated()//funkcja sprawdza czy utworzono wczesniej labirynt
        {
        if (!labirynt.open())
        {
            System.out.println("Nie zczytano z pliku");
            labirynt.save();
        } else
        {
            System.out.println("odczytano z pliku");
        }
        }

    public void przyciski()
        {
        if(Gdx.input.isKeyJustPressed(Input.Keys.ESCAPE))
        Gdx.app.exit();
        
        if(Gdx.input.isKeyJustPressed(Input.Keys.R))
        {
            System.out.println("Reset");
            this.reset();
        }
        }
    public float speed(float speed)//fukcja zmieniająca prędkość symulacji
        {
         if(Gdx.input.isKeyJustPressed(Input.Keys.W))
        {
         System.out.println("Przyspieszamy");
            speed-=0.05;
            if(speed<0.00001)
                return (float)0.00001;
            else
            return speed;
        }
        if(Gdx.input.isKeyJustPressed(Input.Keys.S))
        {
         speed+=0.05;
          System.out.println("Zwalniamy");
          return speed;
        }
        
        return speed;
        }
    public void reset()
        {
        for(Cheese ser:sery)
        {
            ser.resetMe();
        }
         for(Mouse mysz:myszki)
        {
            mysz.resetMe();
        }
         for(Cat kot:kotki)
        {
            kot.resetMe();
        } 
        }

}
