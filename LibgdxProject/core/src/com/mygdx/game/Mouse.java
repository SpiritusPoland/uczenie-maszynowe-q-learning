/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mygdx.game;

import com.badlogic.gdx.graphics.Texture;

/**
 *
 * @author Łukasz
 */
public class Mouse extends GameObject
{
    int hp=100;
    
    public Mouse(int x,int y)
    {
        super(x,y);
        textura=new Texture(CheckSystem()+"mouse.png");
    }
   
    public void move(Labirynt labirynt, Cheese[] sery, Cat[] koty)
        {
        
        if(hp>0)
        {
            
            this.moveObject(labirynt);
            hp-=1;
            if(hp==0)
            {
                this.exists=false;
            }
            
        }
        for (Cheese sery1 : sery)
        {
            if (this.x == sery1.x && this.y == sery1.y && sery1.exists)
            {
                this.hp += sery1.hpRestore;
                sery1.exists = false;
            }
        }
        for(Cat kot:koty)
        {
            if(this.x==kot.x && this.y==kot.y)
            {
                this.hp=0;
                this.exists=false;
            }
        }
        
        }
    public void resetMe()
        {
        this.reset();
        this.hp=100;
        }
    
}
