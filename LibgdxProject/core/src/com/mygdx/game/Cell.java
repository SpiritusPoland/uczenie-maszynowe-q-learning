package com.mygdx.game;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author Łukasz
 */
public class Cell
{
    /*
    Kodowanie będzie przebiegać kolejno :
    gorna
    prawa
    dolna 
    lewa sciana
    
    */
    private boolean gornaSciana;
    private boolean dolnaSciana;
    private boolean lewaSciana;
    private boolean prawaSciana;
    private boolean czyOdwiedzona=false;
    public float wartosc=1;
    private int x;
    private int y;
    Cell(int x, int y)
    {
        gornaSciana=true;
        dolnaSciana=true;
        lewaSciana=true;
        prawaSciana=true;
        this.x=x;
        this.y=y;
    }
    
    public String getProperties()//funkcja zwraca stringa mowiacego o scianach w danej komorce
    {
        String properties;
        if(gornaSciana=true)
        {
            properties="1";
        }
        else
        {
            properties="0";
        }
        
        if(prawaSciana=true)
        {
            properties+="1";
        }
        else
        {
            properties+="0";
        }
        
        if(dolnaSciana=true)
        {
            properties+="1";
        }
        else
        {
            properties+="0";
        }
        
        if(lewaSciana=true)
        {
            properties+="1";
        }
        else
        {
            properties+="0";
        }
        return properties;
    }
    
    public boolean setProperties(String a)
    {
        char znak;
        if(a.length()<4)
            return false;
        else
        {
          if(a.charAt(0)=='1')
          gornaSciana=true;
          else
          gornaSciana=false;
          
          if(a.charAt(1)=='1')
          prawaSciana=true;
          else
          prawaSciana=false;
          
          if(a.charAt(2)=='1')
          dolnaSciana=true;
          else
          dolnaSciana=false;
          
           if(a.charAt(3)=='1')
          lewaSciana=true;
          else
          lewaSciana=false;
          return true;
        }
                    
    }
    public boolean isVisited()
    {
        return czyOdwiedzona;
    }
    public void visit()
    {
        this.czyOdwiedzona=true;     
    }
    public int coordinateX()
    {
        return getX();
    }
    public int coordinateY()
    {
        return getY();
    }


    public void deleteWall(int wall)
    {
        if(wall==0)
            gornaSciana=false;
        if(wall==1)
            prawaSciana=false;
        if(wall==2)
            dolnaSciana=false;
        if(wall==3)
            lewaSciana=false;
    }
    public boolean getWall(int a)
    {
        if(a==0)
            return isGornaSciana();
         if(a==1)
            return isPrawaSciana();
          if(a==2)
            return isDolnaSciana();
           else
            return isLewaSciana();
    }
    public String zapis()
    {
       String a="";
       if(isGornaSciana()==true)
           a+="1";
       else
           a+="0";
       if(isPrawaSciana()==true)
           a+="1";
       else
           a+="0";
       if(isDolnaSciana()==true)
           a+="1";
       else
           a+="0";
       if(isLewaSciana()==true)
           a+="1";
       else
           a+="0";
       return a;
       
    }

    /**
     * @return the gornaSciana
     */
    public boolean isGornaSciana()
        {
        return gornaSciana;
        }

    /**
     * @return the dolnaSciana
     */
    public boolean isDolnaSciana()
        {
        return dolnaSciana;
        }

    /**
     * @return the lewaSciana
     */
    public boolean isLewaSciana()
        {
        return lewaSciana;
        }

    /**
     * @return the prawaSciana
     */
    public boolean isPrawaSciana()
        {
        return prawaSciana;
        }

    /**
     * @return the x
     */
    public int getX()
        {
        return x;
        }

    /**
     * @return the y
     */
    public int getY()
        {
        return y;
        }

}
