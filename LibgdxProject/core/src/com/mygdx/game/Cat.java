/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mygdx.game;

import com.badlogic.gdx.graphics.Texture;

/**
 *
 * @author Łukasz
 */
public class Cat extends GameObject {
    
     public Cat(int x,int y)
    {
        super(x,y);
        textura=new Texture(CheckSystem()+"cat2.png");
    }
     public void move(Labirynt labirynt, Mouse[] myszki)
         {
         this.moveObject(labirynt);
         for(Mouse mysza:myszki)
         {
             if(mysza.x==this.x &&mysza.y==this.y)
             {
                 mysza.exists=false;
                 mysza.hp=0;
             }
         }
         }
     public void resetMe()
         {
         this.reset();
         }
}
