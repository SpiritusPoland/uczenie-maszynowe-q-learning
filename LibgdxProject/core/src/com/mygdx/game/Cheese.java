/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mygdx.game;

import com.badlogic.gdx.graphics.Texture;

/**
 *
 * @author Łukasz
 */
public class Cheese extends GameObject {
    int hpRestore=40; 
    public Cheese(int x,int y)
    {
        super(x,y);
        textura=new Texture(CheckSystem()+"cheese.png");
    }
    
    public void resetMe()
        {
        this.reset();
        this.hpRestore=40;
        }
}
