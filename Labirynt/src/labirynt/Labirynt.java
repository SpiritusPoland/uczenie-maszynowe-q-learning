package labirynt;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
/**
 *
 * @author Łukasz
 */
public class Labirynt {
 Cell tablica[][];
  int wymiarX;
  int wymiarY;
 

  
  Labirynt(int x, int y)
  {
     wymiarX = x;
     wymiarY = y;
     tablica=new Cell[x][y];
     for (int i=0;i<wymiarX;i++)
     {
         for(int j=0;j<wymiarY;j++)
         {
             tablica[i][j]= new Cell(i,j);
         }
     }
  }
  public void generate()//funkcja generuje Labirynt
  {
      int ruch;
      Random losowanie=new Random();
      int poczatkowyX=losowanie.nextInt(this.wymiarX);
      int poczatkowyY=losowanie.nextInt(this.wymiarX);
      List <Cell>lista=new ArrayList<Cell>();
      lista.add(tablica[poczatkowyX][poczatkowyY]);
      lista.get(0).visit();
      while(lista.size()>=1)
      {
         boolean gora=false;
         boolean dol=false;
         boolean lewo=false;
         boolean prawo=false;  
          
          
         int ostatniElement=lista.size();
         int obecnyX=lista.get(lista.size()).coordinateX();//przypisuje współrzędna X z ostatniego elementu listy
         int obecnyY=lista.get(lista.size()).coordinateY();
         do{
             ruch= losowanie.nextInt(4);
             if(gora==true && dol==true && lewo==true && prawo==true)//warunek by pętla losująca ruch nie była nieskończona
             {
                 lista.remove(ostatniElement);
                 break;
             }
         }  
      
         while(canMove(ruch,obecnyX,obecnyY)==false);
         if(gora==true && dol==true && lewo==true && prawo==true)//ponowna iteracja głównego while
             {
                 continue;
             }
         if(ruch==0)//ruch w górę
         {
             tablica[obecnyX][obecnyY+1].visit();
             lista.add(tablica[obecnyX][obecnyY+1]);
             tablica[obecnyX][obecnyY].deleteWall(0);
             tablica[obecnyX][obecnyY+1].deleteWall(2);
         }
         if(ruch==1)//ruch w prawo
         {
             tablica[obecnyX+1][obecnyY].visit();
             lista.add(tablica[obecnyX+1][obecnyY]);
             tablica[obecnyX][obecnyY].deleteWall(1);
             tablica[obecnyX+1][obecnyY].deleteWall(3);
         }
         if(ruch==2)//ruch w dół
         {
             tablica[obecnyX][obecnyY+1].visit();
             lista.add(tablica[obecnyX][obecnyY+1]);
             tablica[obecnyX][obecnyY].deleteWall(2);
             tablica[obecnyX][obecnyY+1].deleteWall(0);
         }
         if(ruch==3)//ruch w lewo
         {
             tablica[obecnyX][obecnyY+1].visit();
             lista.add(tablica[obecnyX][obecnyY+1]);
             tablica[obecnyX][obecnyY].deleteWall(3);
             tablica[obecnyX][obecnyY+1].deleteWall(1);
         }
          
      }
      
  }
  private boolean canMove(int ruch,int x,int y)
  {
     
      Cell cellka;    
      if(ruch ==0)//oznacza przejście do komórki wyżej
          {
              try
              {
              cellka=tablica[x][y+1];    
              if(cellka.isVisited())
              {
                  return false;
              }
              }
              catch(IndexOutOfBoundsException e)
              {
              return false;
              }
          }
          if (ruch==1)//oznacza przejście do komórki po prawej
          {
              try
              {
                cellka=tablica[x+1][y];   
                if(cellka.isVisited())
              {
                  return false;
              }
              }
              catch(IndexOutOfBoundsException e)
              {
              return false;
              }
          }
          if (ruch==2)//oznacza przejście do komórki w dół
          {
              try
              {
               cellka=tablica[x][y-1];    
               if(cellka.isVisited())
              {
                  return false;
              }
              }
              catch(IndexOutOfBoundsException e)
              {
              return false;
              }
          }
          if(ruch==3)//oznacza przejście do komórki w lewo;
          {
              try
              {
               cellka=tablica[x-1][y];  
               if(cellka.isVisited())
              {
                  return false;
              }
              }
              catch(IndexOutOfBoundsException e)
              {
              return false;
              }
          }
          return true;
  }
  
  
 public Cell getCell(int x, int y)
 {
     return tablica[x][y];
 }  
  
}
    

