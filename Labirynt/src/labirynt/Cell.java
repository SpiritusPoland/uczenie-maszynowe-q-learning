package labirynt;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author Łukasz
 */
public class Cell
{
    /*
    Kodowanie będzie przebiegać kolejno :
    gorna
    prawa
    dolna 
    lewa sciana
    
    */
    private boolean gornaSciana;
    private boolean dolnaSciana;
    private boolean lewaSciana;
    private boolean prawaSciana;
    private boolean czyOdwiedzona=false;
    private int x;
    private int y;
    Cell(int x, int y)
    {
        gornaSciana=true;
        dolnaSciana=true;
        lewaSciana=true;
        prawaSciana=true;
        this.x=x;
        this.y=y;
    }
    
    public String getProperties()//funkcja zwraca stringa mowiacego o scianach w danej komorce
    {
        String properties;
        if(gornaSciana=true)
        {
            properties="1";
        }
        else
        {
            properties="0";
        }
        
        if(prawaSciana=true)
        {
            properties+="1";
        }
        else
        {
            properties+="0";
        }
        
        if(dolnaSciana=true)
        {
            properties+="1";
        }
        else
        {
            properties+="0";
        }
        
        if(lewaSciana=true)
        {
            properties+="1";
        }
        else
        {
            properties+="0";
        }
        return properties;
    }
    
    public boolean isVisited()
    {
        return czyOdwiedzona;
    }
    public void visit()
    {
        this.czyOdwiedzona=true;     
    }
    public int coordinateX()
    {
        return x;
    }
    public int coordinateY()
    {
        return y;
    }


    public void deleteWall(int wall)
    {
        if(wall==0)
            gornaSciana=false;
        if(wall==1)
            prawaSciana=false;
        if(wall==2)
            dolnaSciana=false;
        if(wall==3)
            lewaSciana=false;
    }
    public boolean getWall(int a)
    {
        if(a==0)
            return gornaSciana;
         if(a==1)
            return prawaSciana;
          if(a==2)
            return dolnaSciana;
           else
            return lewaSciana;
    }

}
